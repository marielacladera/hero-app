import { useState } from "react"

export const useForm = (initialForm= {}) => {
  const [form, setForm] = useState(initialForm);

  const onChangeValue = ({target}) => {
    const {name, value} = target
    setForm({ 
      ...form, 
      [name]: value 
    })
  }

  const onResetValue = () => {
    setForm(initialForm)
  }
  return {
    ...form,
    form,
    onChangeValue,
    onResetValue
  }
}
