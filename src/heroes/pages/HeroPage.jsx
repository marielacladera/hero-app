import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { getHeroesById } from '../helpers';
import { useMemo } from 'react';

export const HeroPage = () => {

  const navigate = useNavigate();
  const {id} = useParams();

  const hero = useMemo(() => getHeroesById(id), [id]) 

  if(!hero){
    return <Navigate to='/marvel'/>
  }

  const onNavigateBack = () => {
    navigate(-1)
  }

  return (
    <div
      className="row mt-5 animate__animated animate__fadeIn"
    >
      <div
        className="col-3"
      >
        <img
          className='img-thumbnail' 
          src={ `/assets/heroes/${hero.id}.jpg` } 
          alt={ hero.id } 
        />
      </div>
      <div
        className="col-5"
      >
        <h3>{ hero.superhero }</h3>
        <hr />
        <ul className="list-group list-group-flush">
          <li className="list-group-item"><b>Alter ego:</b> { hero.alter_ego }</li>
          <li className="list-group-item"><b>Publisher:</b> { hero.publisher }</li>
          <li className="list-group-item"><b>First appearance:</b> { hero.first_appearance }</li>
        </ul>
        <h4 className='mt-4'>Characters</h4>
        <p>{ hero.characters }</p>
        <button 
          className='btn btn-outline-primary'
          onClick={onNavigateBack}
        >
          Regret
        </button>
      </div>
    </div>
  )
}
