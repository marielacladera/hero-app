import { useLocation, useNavigate } from 'react-router-dom';
import queryString from 'query-string'
import { useForm } from '../../hooks/useForm';
import { HeroCard } from '../components/HeroCard';
import { getHeroesByName } from '../helpers/getHeroesByName';

export const SearchPage = () => {

  const navigate = useNavigate();
  const location = useLocation();
  const { q = '' } = queryString.parse(location.search);
  const heros = getHeroesByName(q)

  const showSearch = (q.length === 0);
  const showError = (q.length > 0) && (heros.length == 0)

  const { searchText, onChangeValue } = useForm({
    searchText: q
  });

  const onSearchSubmit = (event) => {
    event.preventDefault();
    if (searchText.trim().length <= 1) return;
    navigate(`?q=${searchText}`);
  }

  return (
    <>
      <div
        className="row mt-5"
      >
        <div
          className="col-5"
        >
          <form onSubmit={onSearchSubmit}>
            <input
              className="form-control"
              type="text"
              placeholder="Search text"
              name="searchText"
              autoComplete="off"
              value={searchText}
              onChange={onChangeValue}
            />
            <button
              className="btn btn-primary mt-3"
            >
              Search
            </button>
          </form>
        </div>
        <div
          className="col-7"
        >
          <div
            className="alert alert-primary"
            role="alert"
            style={{ display: showSearch ? '' : 'none' }}
          >
            Search a hero
          </div>
          <div
            className="alert alert-danger"
            role="alert"
            style={{ display: showError ? '' : 'none' }}
          >
            No found hero with name <b>{q}</b>
          </div>
          {
            heros.map((hero) =>
              <HeroCard key={hero.id} {...hero} />
            )
          }
        </div>
      </div>
    </>
  )
}
