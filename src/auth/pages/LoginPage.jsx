import { useContext } from "react";
import { useNavigate } from "react-router-dom"
import { AuthContext } from "../context/AuthContext";
import { useForm } from '../../hooks/useForm';

export const LoginPage = () => {
  const navigate = useNavigate();
  const { userName, onChangeValue, onResetValue } = useForm({ userName: '' });
  const { login } = useContext(AuthContext)

  const onLogin = (event) => {
    event.preventDefault();
    const lastPath = localStorage.getItem('lastPath') || '/';
    login(userName);
    onResetValue();
    navigate(lastPath, { replace: true })
  }

  return (
    <div
      className="container mt-5"
    >
      <h1>Login</h1>
      <hr />
      <form className="col-5" onSubmit={onLogin}>
        <input
          className="form-control"
          type="text"
          placeholder="UserName"
          name="userName"
          id="userName"
          autoComplete="off"
          value={userName}
          onChange={onChangeValue}
        />
        <button
          className="btn btn-primary mt-3"
        >
          Login
        </button>
      </form>
    </div>
  )
}
