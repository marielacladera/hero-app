
import { Route, Routes } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import { LoginPage } from '../auth';
import { HeroesRoute } from '../heroes/routes/HeroesRoute';

export const AppRouter = () => {
    return (
        <>
            <Routes >    
                <Route path='login/*' element={ 
                    <PublicRoute>
                        <Routes>
                           <Route path="/*" element={<LoginPage />} />
                        </Routes>
                    </PublicRoute>
                }/>
                <Route path='/*' element={
                    <PrivateRoute>
                        <HeroesRoute />
                    </PrivateRoute>
                }/>
                {/*<Route path="login" element={<LoginPage />} />
                <Route path="/*" element={<HeroesRoute />} />*/}
            </Routes>
        </>
    )
}
